all: bundle.js


bundle.js: js/*.js
	browserify js/*.js -o bundle.js

fiddlebacker.zip: bundle.js
	zip fiddlebacker.zip index.html bundle.js css/* node_modules/abcjs/abcjs-midi.css

deploy: fiddlebacker.zip
	mkdir -p public/ && \
		mv fiddlebacker.zip public/ && \
		cd public/ && \
		unzip fiddlebacker.zip
