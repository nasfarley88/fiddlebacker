import { Map, List } from 'immutable'

/**
 * Return a list of chordBlocks with the chordsToAdd added
 *
 * This should usually be used as:
 * ```
 * const durationOfBar = 4
 * divChords.reduce(partial(divChordsToBlockChordsRdcr, durationOfBar))
 * ```
 *
 * @param {Number} durationOfBar The duration of the bar in beats
 * @param {List} chordBlocks Previously established chordBlocks
 * @param {List} chordsToAdd A list of chords in the current bar
 */
export function divChordsToChordBlocksRdcr (durationOfBar, chordBlocks, chordsToAdd) {
  const lastChordBlock = chordBlocks.last()
  const currentChordBlocks = chordsToAdd.map(x =>
    Map({ symbol: x, duration: durationOfBar / chordsToAdd.size }))

  // if the last chord block has the same symbol as the first of the current
  // chord block, then add the block durations together
  if (lastChordBlock &&
      lastChordBlock.get('symbol') === currentChordBlocks.first().get('symbol')) {
    return List([
      ...chordBlocks.butLast(),
      lastChordBlock.set('duration', lastChordBlock.get('duration') + currentChordBlocks.first().get('duration')),
      ...currentChordBlocks.rest()
    ])
  } else {
    return List([
      ...chordBlocks,
      ...currentChordBlocks
    ])
  }
}
