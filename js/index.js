import '../scss/app.scss'
import '../node_modules/abcjs/abcjs-midi.css'
import '../node_modules/bootstrap/dist/js/bootstrap.bundle'
import * as ABCJS from 'abcjs/midi'
import * as $ from 'jquery'
import { Seq, List, Map } from 'immutable'

import { chordsToAbcString } from './tune'

$('#clicking-button').click(function () {
  var clip = Seq({
    A: List($('.chord.section-a').toArray()).map(obj => List(obj.value.split(' '))),
    B: List($('.chord.section-b').toArray()).map(obj => List(obj.value.split(' '))),
    C: List($('.chord.section-c').toArray()).map(obj => List(obj.value.split(' '))),
    D: List($('.chord.section-d').toArray()).map(obj => List(obj.value.split(' ')))
  })
  const sequence = $('#sequence')[0].value
  const tempo = $('#tempo')[0].value
  const key = $('#key')[0].value
  const timeSig = $('#timeSig')[0].value

  // const tune = new Tune(clip, key, timeSig, sequence, tempo, 'tune.mid')

  // const abcString = tune.toABCString()
  const abcString = chordsToAbcString(
    clip,
    Map({ timeSig: timeSig,
      tempo: tempo,
      key: key,
      sequence: sequence })
  )
  $('#abc-output').text(abcString)
  ABCJS.renderAbc('stave-output', abcString)
  ABCJS.renderMidi('midi-output', abcString, { generateDownload: true })
})
