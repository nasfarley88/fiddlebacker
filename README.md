# fiddlebacker

Backing tracks for fiddle music.

# Development

    npm install # to install the modules
    npm run-script deploy:dev # to deploy the project into `public/`

To use *hot reloading* where the browser automatically reloads parts of the page (e.g. css and js files) when you edit them, use

    npm run-script watch